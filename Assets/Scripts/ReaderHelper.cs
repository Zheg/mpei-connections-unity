﻿public abstract class ReaderHelper
{
    public abstract string getLine();
    
    public abstract bool empty();
}