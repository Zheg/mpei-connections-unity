﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;

public class TwoHandsConnectionsDrawer : MonoBehaviour
{
    public GameObject nodePrefab;
    public GameObject lineRenderPrefab;
    public string mainUser;
    public bool turnOn;
    
    public string path = "All_MPEI_Connections_Hashed.csv";
    
    /// <summary>
    /// ID to GameObject
    /// </summary>
    private readonly Dictionary<string, GameObject> _nodes = new Dictionary<string, GameObject>();

    /// <summary>
    /// Delta scale for each connection to node
    /// </summary>
    private readonly Vector3 _deltaScale = new Vector3(0.1f, 0.1f, 0);

    private ReaderHelper _reader;

    private int firstHandIndex;
    private float firstHandStep;
    private float secondHandStep;
    private List<string> firstHandUsers;
    private List<string> secondHandUsers;
    private float angle;
    private string firstHandUser;
    private int secondHandIndex;
    
    private Stopwatch stopwatch = new Stopwatch();
    
    private void InitializeNode(string id, Vector2 position)
    {
        if (_nodes.ContainsKey(id)) return;
        
        _nodes.Add(id, Procedures.InstantiateNode(nodePrefab, position));
    }

    // Start is called before the first frame update
    void Start()
    {
        _reader = new FileReaderHelper(path);
        firstHandIndex = 0;
        firstHandUsers = UserConnections(mainUser, path);
        InitializeNode(mainUser, ScreenUtils.Center);
        firstHandStep = Mathf.PI * 2 / firstHandUsers.Count;
        NextFirstHandUser();
    }

    public static List<string> UserConnections(string id, string path)
    {
        var result = new List<string>();
        using (var sr = new StreamReader(path))
        {
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                var users = line.Split(';');
                if (id == users[0])
                    result.Add(users[1]);
                else if (id == users[1])
                    result.Add(users[0]);
            }
        }

        return result;
    }
    
    public void NextFirstHandUser()
    {
        if (firstHandIndex >= firstHandUsers.Count) return;
        
        firstHandUser = firstHandUsers[firstHandIndex];
        angle = firstHandIndex * firstHandStep;
        secondHandUsers = UserConnections(firstHandUser, path);
        secondHandStep = firstHandStep / secondHandUsers.Count;
        secondHandIndex = 0;
        if (turnOn)
        {
            InitializeNode(firstHandUser, new Vector2(Mathf.Cos(angle), Mathf.Sin(angle))
                * (ScreenUtils.Radius / 2) + ScreenUtils.Center);
            Procedures.DrawConnection(_nodes[mainUser], _nodes[firstHandUser], lineRenderPrefab);
        }

        ++firstHandIndex;
    }

    // Update is called once per frame
    void Update()
    {
        if (!turnOn) return;
        if (firstHandIndex >= firstHandUsers.Count) return;

        if (secondHandIndex >= secondHandUsers.Count)
        {
            NextFirstHandUser();
        }

        var deltaAngle = -firstHandStep / 2 + secondHandStep * secondHandIndex;
 
        var secondUser = secondHandUsers[secondHandIndex++];
        if (firstHandUsers.Contains(secondUser))
        {
            if (_nodes.ContainsKey(secondUser))
            {
                Procedures.DrawConnection(_nodes[secondUser], _nodes[firstHandUser], lineRenderPrefab);
            }

            return;
        }
        
        InitializeNode(secondUser,
            new Vector2(Mathf.Cos(angle + deltaAngle), Mathf.Sin(angle + deltaAngle))
            * ScreenUtils.Radius + ScreenUtils.Center);
        Procedures.DrawConnection(_nodes[firstHandUser], _nodes[secondUser], lineRenderPrefab);
    }
    
    // public void BuildTwoHandsConnections(string id)
    // {
    //     var oneHand = TwoHandsConnections.UserConnections(id, path);
    //     var halfRadius = _radius / 2;
    //     InitializeNode(id, _center);
    //     var step = Mathf.PI * 2 / oneHand.Count;
    //     var angle = 0f;
    //     for (var index = 0; index < oneHand.Count; index++)
    //     {
    //         var user = oneHand[index];
    //         InitializeNode(user, new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * halfRadius + _center);
    //         DrawConnection(id, user);
    //         var secondHand = TwoHandsConnections.UserConnections(user, path);
    //         var secondStep = step / secondHand.Count;
    //         var deltaAngle = -step / 2;
    //         for (var i = 0; i < secondHand.Count; i++)
    //         {
    //             var secondUser = secondHand[i];
    //             InitializeNode(secondUser,
    //                 new Vector2(Mathf.Cos(angle + deltaAngle), Mathf.Sin(angle + deltaAngle))
    //                 * _radius + _center);
    //             DrawConnection(user, secondUser);
    //             deltaAngle += secondStep;
    //         }
    //
    //         angle += step;
    //     }
    // }
}
