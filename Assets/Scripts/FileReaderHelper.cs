﻿using System;
using System.IO;

public class FileReaderHelper : ReaderHelper
{
    private StreamReader sr;
    private bool correct;

    public FileReaderHelper(string path)
    {
        try
        {
            sr = new StreamReader(path);
            correct = true;
        }
        catch (Exception ex)
        {
            Console.Error.WriteLine("Could not find file '" + path + "'.\n" + ex.Message);
            sr = null;
            correct = false;
        }
    }

    public override string getLine()
    {
        return empty() ? "" : sr.ReadLine();
    }

    public override bool empty()
    {
        return correct ? sr.Peek() == -1 : true;
    }

    ~FileReaderHelper()
    {
        if (correct)
        {
            sr.Close();
        }
    }
}