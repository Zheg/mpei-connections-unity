﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

public class GameManagerScript : MonoBehaviour
{
    public bool turnOn;
    public GameObject nodePrefab;
    public GameObject lineRenderPrefab;
    
    public bool onCircle;

    public string path = "All_MPEI_Connections_Hashed.csv";
    
    /// <summary>
    /// ID to GameObject
    /// </summary>
    private readonly Dictionary<string, GameObject> _nodes = new Dictionary<string, GameObject>();
    
    /// <summary>
    /// ID to Color
    /// </summary>
    private readonly Dictionary<string, Color> _colors = new Dictionary<string, Color>();
    
    /// <summary>
    /// Delta scale for each connection to node
    /// </summary>
    private readonly Vector3 _deltaScale = new Vector3(0.1f, 0.1f, 0);

    private ReaderHelper _reader;
    private static float _radius;
    private static Vector2 _center;

    // Start is called before the first frame update
    void Start()
    {
        _reader = new FileReaderHelper(path);
        _radius = Math.Min(ScreenUtils.ScreenTop - ScreenUtils.ScreenBottom,
            ScreenUtils.ScreenRight - ScreenUtils.ScreenLeft) / 2;
        _center = new Vector2((ScreenUtils.ScreenRight + ScreenUtils.ScreenLeft) / 2,
            (ScreenUtils.ScreenTop + ScreenUtils.ScreenBottom) / 2);
    }

    private void InitializeNode(string node)
    {
        var position = onCircle ? GetRandomPointOnCircle() : GetRandomPoint(); 
        _nodes.Add(node, Instantiate(nodePrefab, position, Quaternion.identity));
        _colors.Add(node, Random.ColorHSV());
    }

    private void InitializeNode(string id, Vector2 position)
    {
        if (!_nodes.ContainsKey(id))
            _nodes.Add(id, Instantiate(nodePrefab, position, Quaternion.identity));
    }

    private void DrawConnection(string node1, string node2)
    {
        if (!_nodes.ContainsKey(node1))
            InitializeNode(node1);
        if (!_nodes.ContainsKey(node2))
            InitializeNode(node2);
        _nodes[node1].transform.localScale += _deltaScale;
        _nodes[node2].transform.localScale += _deltaScale;
        var lineRender = Instantiate(lineRenderPrefab, Vector3.zero, Quaternion.identity).GetComponent<LineRenderer>();
        lineRender.positionCount = 2;
        // lineRender.startColor = _colors[node1];
        // lineRender.endColor = _colors[node1];
        lineRender.SetPosition(0, _nodes[node1].transform.position);
        lineRender.SetPosition(1, _nodes[node2].transform.position);
    }

    public static Vector2 GetRandomPoint()
    {
        var x = Random.Range(ScreenUtils.ScreenLeft, ScreenUtils.ScreenRight);
        var y = Random.Range(ScreenUtils.ScreenBottom, ScreenUtils.ScreenTop);
        return new Vector2(x, y);
    }

    public static Vector2 GetRandomPointOnCircle()
    {
        var angle = Random.Range(0, (float)(2 * Math.PI));
        var position = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle)) * _radius + _center;
        return position;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (!turnOn) return;
        
        if (_reader.empty()) return;
        var line = _reader.getLine();
        var friends = line.Split(';');
        DrawConnection(friends[0], friends[1]);
    }

   
}
