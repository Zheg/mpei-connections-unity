﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class THCDv2 : MonoBehaviour
{
    public bool turnOn;
    public GameObject nodePrefab;
    public GameObject lineRenderPrefab;
    public string mainUser;
    public int drawsPerFrame = 10;
    public bool dynamicConnections;
    
    public string path = "All_MPEI_Connections_Hashed.csv";
    
    /// <summary>
    /// ID to GameObject
    /// </summary>
    private readonly Dictionary<string, GameObject> _nodes = new Dictionary<string, GameObject>();

    /// <summary>
    /// Delta scale for each connection to node
    /// </summary>
    private readonly Vector3 _deltaScale = new Vector3(0.1f, 0.1f, 0);

    private ReaderHelper _reader;

    private HashSet<string>.Enumerator firstHandUsersEnumerator;
    private HashSet<string>.Enumerator secondHandUsersEnumerator;
    private HashSet<string>.Enumerator firstHandConnectionsEnumerator;
    private HashSet<string>.Enumerator secondHandConnectionsEnumerator;
    private float firstHandStep;
    private float secondHandStep;
    private HashSet<string> firstHandConnections;
    private HashSet<string> secondHandConnections;
    private HashSet<string> firstHandUsers;
    private HashSet<string> secondHandUsers;
    private float angle;
    private string firstHandUser;

    private Text statistics;
    
    private Stopwatch stopwatch = new Stopwatch();
    
    private void InitializeNode(string id, Vector2 position)
    {
        if (_nodes.ContainsKey(id)) return;
        
        _nodes.Add(id, Procedures.InstantiateNode(nodePrefab, position));
        _nodes[id].name = id;
    }

    // Start is called before the first frame update
    void Start()
    {
        _reader = new FileReaderHelper(path);
        (firstHandConnections, firstHandUsers) = 
            UsersConnections(new HashSet<string>() {mainUser}, new HashSet<string>(), path);
        (secondHandConnections, secondHandUsers) = 
            UsersConnections(firstHandUsers, new HashSet<string>() {mainUser}, path);
        InitializeNode(mainUser, ScreenUtils.Center);
        _nodes[mainUser].GetComponent<SpriteRenderer>().color = Color.green;
        
        firstHandStep = Mathf.PI * 2 / firstHandConnections.Count;
        secondHandStep = Mathf.PI * 2 / secondHandUsers.Count;
        
        firstHandConnectionsEnumerator = firstHandConnections.GetEnumerator();
        firstHandUsersEnumerator = firstHandUsers.GetEnumerator();
        secondHandConnectionsEnumerator = secondHandConnections.GetEnumerator();
        secondHandUsersEnumerator = secondHandUsers.GetEnumerator();

        statistics = GameObject.FindWithTag("Text").GetComponent<Text>();
        // NextFirstHandUser();
    }

    public static Tuple<HashSet<string>, HashSet<string>> UsersConnections
        (HashSet<string> users, HashSet<string> exclude, string path)
    {
        HashSet<string> connectionsResult = new HashSet<string>();
        HashSet<string> usersResult = new HashSet<string>();
        using (var sr = new StreamReader(path))
        {
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                var friends = line.Split(';');
                foreach (var id in users)
                {
                    if ((id != friends[0] && id != friends[1]) 
                        || exclude.Contains(friends[0]) || exclude.Contains(friends[1])) continue;
                    
                    connectionsResult.Add(line);
                    if (!users.Contains(friends[0]))
                        usersResult.Add(friends[0]);
                        
                    if (!users.Contains(friends[1]))
                        usersResult.Add(friends[1]);
                }
            }
        }

        return new Tuple<HashSet<string>, HashSet<string>>(connectionsResult, usersResult);
    }

    // Update is called once per frame
    void Update()
    {
        if (!turnOn) return;

        bool toReturn = false;

        for (int i = 0; i < drawsPerFrame; i++)
        {
            if (firstHandUsersEnumerator.MoveNext())
            {
                InitializeNode(firstHandUsersEnumerator.Current, 
                    new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * (ScreenUtils.Radius / 2) 
                    + ScreenUtils.Center);
                angle += firstHandStep;
                toReturn = true;
            }
            else break;
        }
        if (toReturn) return;
        statistics.text = "One hand friends: " + firstHandUsers.Count + "\n";
        
        for (int i = 0; i < drawsPerFrame; i++)
        {
            if (secondHandUsersEnumerator.MoveNext())
            {
                InitializeNode(secondHandUsersEnumerator.Current, 
                    new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * ScreenUtils.Radius 
                    + ScreenUtils.Center);
                angle += secondHandStep;
                toReturn = true;
            }
            else break;
        }
        if (toReturn) return;
        statistics.text += "Two hand friends: " + secondHandUsers.Count + "\n";

        
        for (int i = 0; i < drawsPerFrame; i++)
        {
            if (firstHandConnectionsEnumerator.MoveNext())
            {
                var friends = firstHandConnectionsEnumerator.Current?.Split(';');
                Procedures.DrawConnection(_nodes[friends[0]], _nodes[friends[1]], 
                    lineRenderPrefab, dynamicConnections);
                toReturn = true;
            }
            else break;
        }
        if (toReturn) return;
        statistics.text += "One hand connections: " + firstHandConnections.Count + "\n";

        for (int i = 0; i < drawsPerFrame; i++)
        {
            if (secondHandConnectionsEnumerator.MoveNext())
            {
                var friends = secondHandConnectionsEnumerator.Current?.Split(';');
                Procedures.DrawConnection(_nodes[friends[0]], _nodes[friends[1]], 
                    lineRenderPrefab, dynamicConnections);
                toReturn = true;
            }
            else break;
        }
        if (toReturn) return;
        
        statistics.text += "Two hand connections: " + secondHandConnections.Count + "\n";

    }
}
