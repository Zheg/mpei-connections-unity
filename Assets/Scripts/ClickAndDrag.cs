﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickAndDrag : MonoBehaviour
{

    private Vector3 _startPos;

    private bool _isBeingHeld;

    private Camera _camera;

    // Start is called before the first frame update
    void Start()
    {
        _camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isBeingHeld) return;
        
        var mousePos = _camera.ScreenToWorldPoint(Input.mousePosition);

        transform.localPosition = mousePos - _startPos;
    }

    void OnMouseDown()
    {
        var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        _startPos = mousePos - transform.localPosition;
        
        _isBeingHeld = true;
    }

    void OnMouseUp()
    {
        _isBeingHeld = false;
    }
}
