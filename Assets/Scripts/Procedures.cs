﻿
using System;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public static class Procedures
{

    /// <summary>
    /// Delta scale that adds for each connection to node
    /// </summary>
    public static readonly Vector3 DeltaScale = new Vector3(0.1f, 0.1f, 0);

    /// <summary>
    /// Instantiate nodePrefab at random point at the screen
    /// if onCircle is TRUE then random point is selected at the circle with maximum radius that fits the screen
    /// </summary>
    /// <param name="nodePrefab">Prefab for GameObject</param>
    /// <param name="onCircle"></param>
    /// <returns>Instantiated GameObject</returns>
    public static GameObject InstantiateNodeAtRandomPoint(GameObject nodePrefab, bool onCircle = false)
    {
        var position = onCircle ? GetRandomPointOnCircle() : GetRandomPoint(); 
        return Object.Instantiate(nodePrefab, position, Quaternion.identity);
    }
    
    /// <summary></summary>
    /// <returns>Random point at the screen</returns>
    public static Vector2 GetRandomPoint()
    {
        var x = Random.Range(ScreenUtils.ScreenLeft, ScreenUtils.ScreenRight);
        var y = Random.Range(ScreenUtils.ScreenBottom, ScreenUtils.ScreenTop);
        return new Vector2(x, y);
    }
    /// <summary></summary>
    /// <returns>Random point at circle with maximum radius that fits the screen</returns>
    public static Vector2 GetRandomPointOnCircle()
    {
        var angle = Random.Range(0, (float)(2 * Mathf.PI));
        var position = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * ScreenUtils.Radius + ScreenUtils.Center;
        return position;
    }
    
    /// <summary>
    /// Instantiate nodePrefab at position
    /// </summary>
    /// <param name="nodePrefab">Prefab for GameObject</param>
    /// <param name="position">Position where node would be instantiated</param>
    /// <returns>Instantiated GameObject</returns>
    public static GameObject InstantiateNode(GameObject nodePrefab, Vector2 position)
    {
        return Object.Instantiate(nodePrefab, position, Quaternion.identity);
    }
    
    public static void DrawConnection(GameObject node1, GameObject node2, GameObject lineRenderPrefab, bool dynamic = false)
    {
        node1.transform.localScale += DeltaScale;
        node2.transform.localScale += DeltaScale;
        var lineRendererObject = Object.Instantiate(lineRenderPrefab, Vector3.zero, Quaternion.identity);
        lineRendererObject.GetComponent<LineRenderScript>().dynamic = dynamic;
        if (!dynamic)
        {
            var lineRenderer = lineRendererObject.GetComponent<LineRenderer>();
            lineRenderer.positionCount = 2;
            lineRenderer.SetPosition(0, node1.transform.position);
            lineRenderer.SetPosition(1, node2.transform.position);
        }
        else
        {
            var lineRenderer = lineRendererObject.GetComponent<LineRenderScript>();
            lineRenderer.go1 = node1;
            lineRenderer.go2 = node2;
        }
    }
}