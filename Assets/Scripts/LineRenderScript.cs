﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRenderScript : MonoBehaviour
{
    public bool dynamic;
    public GameObject go1;
    public GameObject go2;
    

    private LineRenderer _lineRenderer;
    // Start is called before the first frame update
    void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!dynamic) return;
        Vector3[] positions = new Vector3[2];
        positions[0] = go1.transform.position;
        positions[1] = go2.transform.position;
        _lineRenderer.SetPositions(positions);
    }
}
